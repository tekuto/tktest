package tktest

type Name string

func ( THIS *Name ) GetName(
) string {
    return string( *THIS )
}
