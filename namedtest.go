package tktest

type NamedTest interface {
    Test
    GetName() string
}
