package tktest

import (
    "github.com/golang/mock/gomock"
    "testing"
)

func RunWithGomock(
    _t *testing.T,
    _TEST TestWithGomock,
) {
    gomockController := gomock.NewController( _t )
    defer gomockController.Finish()

    _TEST.Test(
        _t,
        gomockController,
    )
}

func RunWithGomockParallel(
    _t *testing.T,
    _TEST TestWithGomock,
) {
    _t.Parallel()

    RunWithGomock(
        _t,
        _TEST,
    )
}

func RunWithGomockTableDriven(
    _t *testing.T,
    _TESTS ... NamedTestWithGomock,
) {
    for _, TEST := range _TESTS {
        _t.Run(
            TEST.GetName(),
            func( _t *testing.T ) {
                RunWithGomock(
                    _t,
                    TEST,
                )
            },
        )
    }
}

func RunWithGomockTableDrivenParallel(
    _t *testing.T,
    _TESTS ... NamedTestWithGomock,
) {
    _t.Parallel()

    for _, TEST := range _TESTS {
        TEST := TEST

        _t.Run(
            TEST.GetName(),
            func( _t *testing.T ) {
                RunWithGomockParallel(
                    _t,
                    TEST,
                )
            },
        )
    }
}
