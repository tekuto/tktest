package tktest

import (
    "testing"
)

func Run(
    _t *testing.T,
    _TEST Test,
) {
    _TEST.Test( _t )
}

func RunParallel(
    _t *testing.T,
    _TEST Test,
) {
    _t.Parallel()

    Run(
        _t,
        _TEST,
    )
}

func RunTableDriven(
    _t *testing.T,
    _TESTS ... NamedTest,
) {
    for _, TEST := range _TESTS {
        _t.Run(
            TEST.GetName(),
            func( _t *testing.T ) {
                Run(
                    _t,
                    TEST,
                )
            },
        )
    }
}

func RunTableDrivenParallel(
    _t *testing.T,
    _TESTS ... NamedTest,
) {
    _t.Parallel()

    for _, TEST := range _TESTS {
        TEST := TEST

        _t.Run(
            TEST.GetName(),
            func( _t *testing.T ) {
                RunParallel(
                    _t,
                    TEST,
                )
            },
        )
    }
}
