package run

import (
    "bitbucket.org/tekuto/tktest"
    "testing"
    "time"
)

func Sum(
    _I1 int,
    _I2 int,
) int {
    time.Sleep( time.Millisecond * 10 )

    return _I1 + _I2
}

type testSum struct {
    expected int
    i1 int
    i2 int
}

func ( THIS *testSum ) Test( _t *testing.T ) {
    test(
        _t,
        THIS.expected,
        THIS.i1,
        THIS.i2,
    )
}

func test(
    _t *testing.T,
    _EXPECTED int,
    _I1 int,
    _I2 int,
) {
    if RESULT := Sum(
        _I1,
        _I2,
    ); RESULT != _EXPECTED {
        _t.Errorf(
            "Sum( %d, %d ) = %d; %d が期待される",
            _I1,
            _I2,
            RESULT,
            _EXPECTED,
        )
    }
}

func TestRun( _t *testing.T ) {
    tktest.Run(
        _t,
        &testSum{
            3,
            1,
            2,
        },
    )
}

func TestRunParallel( _t *testing.T ) {
    tktest.RunParallel(
        _t,
        &testSum{
            3,
            1,
            2,
        },
    )
}

type namedTestSum struct {
    tktest.Name
    expected int
    i1 int
    i2 int
}

func ( THIS *namedTestSum ) Test( _t *testing.T ) {
    test(
        _t,
        THIS.expected,
        THIS.i1,
        THIS.i2,
    )
}

func TestRunTableDriven( _t *testing.T ) {
    tktest.RunTableDriven(
        _t,
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト1" ),
            3,
            1,
            2,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト2" ),
            5,
            2,
            3,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト3" ),
            7,
            3,
            4,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト4" ),
            9,
            4,
            5,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト5" ),
            11,
            5,
            6,
        },
    )
}

func TestRunTableDrivenParallel( _t *testing.T ) {
    tktest.RunTableDrivenParallel(
        _t,
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト並列1" ),
            3,
            1,
            2,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト並列2" ),
            5,
            2,
            3,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト並列3" ),
            7,
            3,
            4,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト並列4" ),
            9,
            4,
            5,
        },
        &namedTestSum{
            tktest.Name( "テーブルドリブンテスト並列5" ),
            11,
            5,
            6,
        },
    )
}
