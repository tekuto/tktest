package runwithgomock

import (
    "bitbucket.org/tekuto/tktest"
    "github.com/golang/mock/gomock"
    "testing"
)

type testGet struct {
    expected string
    setValue string
}

func ( THIS *testGet ) Test(
    _t *testing.T,
    _gomockController *gomock.Controller,
) {
    test(
        _t,
        _gomockController,
        THIS.expected,
        THIS.setValue,
    )
}

func test(
    _t *testing.T,
    _gomockController *gomock.Controller,
    _EXPECTED string,
    _SET_VALUE string,
) {
    mockInterface := NewMockInterface( _gomockController )

    EXPECT := mockInterface.EXPECT()

    EXPECT.Set( gomock.Any() ).Do(
        func( _value string ) {
            EXPECT.Get().Return( _value )
        },
    )

    mockInterface.Set( _SET_VALUE )

    if RESULT := mockInterface.Get(); RESULT != _EXPECTED {
        _t.Errorf(
            `Get() = %s; "%s" が期待される`,
            RESULT,
            _EXPECTED,
        )
    }
}

func TestRunWithGomock( _t *testing.T ) {
    tktest.RunWithGomock(
        _t,
        &testGet{
            "てすと1",
            "てすと1",
        },
    )
}

func TestRunWithGomockParallel( _t *testing.T ) {
    tktest.RunWithGomockParallel(
        _t,
        &testGet{
            "てすと2",
            "てすと2",
        },
    )
}

type namedTestGet struct {
    tktest.Name
    expected string
    setValue string
}

func ( THIS *namedTestGet ) Test(
    _t *testing.T,
    _gomockController *gomock.Controller,
) {
    test(
        _t,
        _gomockController,
        THIS.expected,
        THIS.setValue,
    )
}

func TestRunWithGomockTableDriven( _t *testing.T ) {
    tktest.RunWithGomockTableDriven(
        _t,
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト1" ),
            "てすと3",
            "てすと3",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト2" ),
            "てすと4",
            "てすと4",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト3" ),
            "てすと5",
            "てすと5",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト4" ),
            "てすと6",
            "てすと6",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト5" ),
            "てすと7",
            "てすと7",
        },
    )
}

func TestRunWithGomockTableDrivenParallel( _t *testing.T ) {
    tktest.RunWithGomockTableDrivenParallel(
        _t,
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト並列1" ),
            "てすと8",
            "てすと8",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト並列2" ),
            "てすと9",
            "てすと9",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト並列3" ),
            "てすと10",
            "てすと10",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト並列4" ),
            "てすと11",
            "てすと11",
        },
        &namedTestGet{
            tktest.Name( "テーブルドリブンテスト並列5" ),
            "てすと12",
            "てすと12",
        },
    )
}
