package tktest

import (
    "github.com/golang/mock/gomock"
    "testing"
)

type TestWithGomock interface {
    Test(
        *testing.T,
        *gomock.Controller,
    )
}
