package tktest

type NamedTestWithGomock interface {
    TestWithGomock
    GetName() string
}
